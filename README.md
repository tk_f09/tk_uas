# TK_F09

## Nama Anggota

- Alaida Arifah Zahra Akmal
- Ezra Pasha Ramadhansyah - 2006597872
- Luthfi Alnazhary - 2006597506
- Arinil Haq - 2006597430
- Joya Ruth Amanda Pane
- Dzikri Qalam Hatorangan - 2006595854
- M Fadhil Mustari - 2006597342

## Cerita Aplikasi

Kami memutuskan untuk membuat aplikasi seperti Peduli Lindungi. Dalam aplikasi kami, user dapat mengetahui informasi seputar covid seperti jumlah kasus yang sembuh, meninggal, dan yang terinfeksi berdasarkan domisili. Terdapat juga search bar agar user bisa mencari data terkait berdasarkan domisili. Selain itu, pada aplikasi kami terdapat portal informasi mengenai kumpulan postingan yang berguna selama pandemi COVID-19.  Selain itu, user dapat mendaftarkan dirinya untuk mendapatkan vaksin lewat aplikasi kami. Kami harap aplikasi kami dapat bermanfaat dalam membantu penyebaran informasi seputar COVID-19 dan menekan laju penyebaran COVID-19.

## Link Download APK

https://drive.google.com/drive/folders/1tTEaYCaJLUR0Tzj9c1ObUM0vWDYWx4FD?usp=sharing

## Daftar Modul

1. Halaman login untuk authentication.  
Pada halaman Login ini berfungsi sebagaimananya halaman yang akan digunakan untuk Login dan Registrasi. Sama juga dengan halaman pada Web yang akan melakukan login terlebih dahulu dan juga melakukan registrasi sebelum melakukan Login.

2. Halaman beranda.  
Dikarenakan halaman beranda memakai API dari luar (bukan API dari website UTS) maka nantinya di tampilan beranda akan memakai “Future” widget dengan fetch url-nya sesuai API luar tersebut. Untuk implementasi codingan-nya akan mirip seperti di PowerPoint “10 - Navigation, Networking and Integration” halaman 16. Lalu, karena data yang dikirim berupa JSON maka Dart menyediakan “tools” untuk meng-convert JSON string yang diterima menjadi bentuk Map<String, dynamic>.

3. Halaman hasil search.  
Halaman hasil search akan berfungsi sama seperti di dalam web. Bedanya adalah untuk tugas akhir, hasil search akan diambil dari data yang sudah ada di web tengah semester. Untuk tampilannya akan sedikit berbeda. Dalam tugas tengah semester halaman search dan hasil berbeda, sementara untuk tugas akhir halaman akan menampilkan semua hasil dengan search bar di atas yang akan memfilter hasil yang ada agar sesuai. 

4. Halaman form pendaftaran vaksin.  
Halaman ini digunakan untuk mendaftarkan diri untuk vaksinasi. Pada halaman ini akan berisi form pendaftaran vaksin yang digunakan untuk mengisi data diri yang diperlukan, Data- data yang diminta adalah nama lengkap, tanggal lahir, NIK, vaksinasi yang keberapa, lokasi vaksinasi, dan tanggal untuk vaksinasi. Data-data ini akan dibutuhkan dalam membuat sertifikat vaksin. 

5. Halaman sertifikasi vaksin.  
Halaman ini akan menampilkan sertifikasi vaksin dari pengguna setelah melakukan pendaftaran vaksin melalui halaman form pendaftaran vaksin. Pada halaman ini, akan terdapat foto sertifikat hasil vaksin, nama, NIK, dan keterangan vaksin. Keterangan vaksin tersebut meliputi jenis vaksin, lokasi, dan tanggal vaksinasi tersebut dilakukan.

6. Halaman form postingan informasi seputar COVID-19.  
Halaman ini akan berisi form untuk postingan informasi seputar COVID-19, yaitu judul, sub judul, konten, dan alamat URL images dari postingan. Input yang didapatkan melalui masing-masing form input text setelahnya akan dipanggil URL dari django service yang dibuat melalui method POST yang kemudian disimpan di database. 

7. Halaman postingan informasi seputar COVID-19.  
Halaman ini akan berisi berbagai macam berita informasi seputar COVID-19. Konten pada halaman ini berasal dari halaman form postingan tempat user memasukkan konten yang ingin ditampilkan pada halaman ini. Pada halaman ini akan ditampilkan judul, overview singkat mengenai konten yang akan ditampilkan, dan tentu saja konten itu sendiri. Proses pengambilan data yang telah diinput oleh user melalui halaman form postingan. 

## User Persona

Persona 1 - Bayu: Bayu adalah seorang pria berumur kisaran 30 tahun yang seringkali mengakses internet untuk mencari informasi-informasi yang mungkin berguna untuk dirinya. Dia ingin mencari informasi data-data mengenai update kasus COVID-19.

Persona 2 - Mawar: Mawar adalah seorang Ibu-ibu rumah tangga berusia 43 tahun yang sering menghabiskan waktu luangnya untuk berselancar di internet dan melihat konten-konten menarik. Mawar suka bingung apabila ingin mendapatkan tips-tips mutakhir agar dia dan sekeluarga bisa aman selama pandemi. 

Persona 3 - Angga : Angga merupakan pekerja kantoran di wilayah kuningan yang berusia 25 tahun. Dia ingin mengikuti vaksinasi dan mencoba mendaftarkan dirinya melalui pendaftaran vaksinasi online melalui aplikasi



