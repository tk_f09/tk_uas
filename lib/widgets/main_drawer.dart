import 'package:flutter/material.dart';
import 'package:tk_uas/screens/SertifikatVaksin_screen.dart';
import 'package:tk_uas/screens/pageLain_screen.dart';

class MainDrawer extends StatelessWidget {
  Widget buildListTile(String title, IconData icon, Function tapHandler) {
    return ListTile(
      leading: Icon(
        icon,
        size: 26,
      ),
      title: Text(
        title,
        style: TextStyle(
          fontFamily: 'RobotoCondensed',
          fontSize: 24,
          fontWeight: FontWeight.bold,
        ),
      ),
      onTap: tapHandler,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        children: <Widget>[
          Container(
            height: 120,
            width: double.infinity,
            padding: EdgeInsets.all(20),
            alignment: Alignment.centerLeft,
            color: Theme.of(context).accentColor,
            child: Text(
              'TK F09',
              style: TextStyle(
                  fontWeight: FontWeight.w900,
                  fontSize: 30,
                  color: Theme.of(context).primaryColor),
            ),
          ),
          SizedBox(
            height: 20,
          ),
          buildListTile('Beranda', Icons.home, () {
            Navigator.of(context).pushReplacementNamed('/');
          }),
          buildListTile('Cari Informasi', Icons.search, () {
            Navigator.of(context).pushReplacementNamed('/search');
          }),
          buildListTile("Informasi", Icons.search, () {
            Navigator.of(context).pushReplacementNamed('/information');
          }),
          buildListTile('Tambah Postingan', Icons.add, () {
            Navigator.of(context).pushReplacementNamed('/form-post');
          }),
          buildListTile('Daftar Vaksin', Icons.add, () {
            Navigator.of(context).pushReplacementNamed('/input-vaksin');
          }),
          buildListTile('Sertifikat Vaksin', Icons.assignment_ind, () {
            Navigator.of(context).pushReplacementNamed('/sertifikat-vaksin');
          }),
          buildListTile('Logout', Icons.logout, () {
            Navigator.of(context).pushReplacementNamed('/login');
          }),
        ],
      ),
    );
  }
}
