import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:tk_uas/pages/login_page.dart';

import 'pages/splash_screen.dart';
import './screens/tabs_screen.dart';
import 'screens/home_screen.dart';
import 'screens/pageLain_screen.dart';
import 'screens/search_screen.dart';
import 'package:tk_uas/screens/information_screen.dart';
import 'screens/form-postingan/formPostinganPage.dart';
import 'screens/input_vaksin.dart';
import 'screens/sertifikatVaksin_screen.dart';

void main() {
  runApp(LoginUiApp());
}

class LoginUiApp extends StatelessWidget {
  Color _primaryColor = HexColor('#40E0FF');
  Color _accentColor = HexColor('#36BFFF');

  // Design color
  // Color _primaryColor= HexColor('#FFC867');
  // Color _accentColor= HexColor('#FF3CBD');

  // Our Logo Color
  // Color _primaryColor= HexColor('#D44CF6');
  // Color _accentColor= HexColor('#5E18C8');

  // Our Logo Blue Color
  //Color _primaryColor= HexColor('#651BD2');
  //Color _accentColor= HexColor('#320181');

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Login TK F09',
      theme: ThemeData(
        primaryColor: _primaryColor,
        accentColor: _accentColor,
        scaffoldBackgroundColor: Colors.grey.shade100,
        primarySwatch: Colors.grey,
      ),
      // home: CategoriesScreen(),
      initialRoute: '/', // default is '/'
      routes: {
        '/': (ctx) => TabsScreen(),
        PageLain.routeName: (ctx) => PageLain(),
        '/search': (ctx) => SearchPage(),
        '/information': (ctx) => Information(),
        '/form-post': (ctx) => Formulir(),
        '/sertifikat-vaksin': (ctx) => SertifikatVaksin(),
        '/input-vaksin': (ctx) => InputVaksin(),
        '/login': (ctx) => LoginPage(),
      },
      onGenerateRoute: (settings) {
        print(settings.arguments);
      },
      onUnknownRoute: (settings) {
        return MaterialPageRoute(
          builder: (ctx) => HomeScreen(),
        );
      },
      //home: SplashScreen(title: 'Login TKF09'),
    );
  }
}
