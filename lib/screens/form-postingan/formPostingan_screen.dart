import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tk_uas/screens/form-postingan/formPostinganPage.dart';

class FormulirPostingan extends StatelessWidget {
  //const FormulirPostingan({Key? key}) : super(key:key);

  @override
  Widget build(BuildContext) {
    return ChangeNotifierProvider(
      create: (context) => FormPostinganProvider(),
      child: MaterialApp(
        title: 'Form Postingan Informasi',
        home: Formulir(),
      ),
    );
      
  }
}

