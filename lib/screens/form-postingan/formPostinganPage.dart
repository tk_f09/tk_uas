// ignore_for_file: unnecessary_new, prefer_const_constructors, use_key_in_widget_constructors, avoid_print, non_constant_identifier_names, prefer_final_fields
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';
import 'package:tk_uas/widgets/main_drawer.dart';


class Formulir extends StatefulWidget {
  //const Formulir({ Key? key }) : super(key: key);
  
  @override
  _FormulirState createState() => _FormulirState();
}



class _FormulirState extends State<Formulir> {

  final _formKey = GlobalKey<FormState>();
  final TextEditingController _titleController = TextEditingController();
  final TextEditingController _subtitleController = TextEditingController();
  final TextEditingController _contentController = TextEditingController();
  final TextEditingController _imagesController = TextEditingController();

  void onAdd() {
    final String titleVal = _titleController.text;
    final String subTitleVal = _subtitleController.text;
    final String contentVal = _contentController.text;
    final String imageVal = _imagesController.text;

    if (titleVal.isNotEmpty && subTitleVal.isNotEmpty
    && contentVal.isNotEmpty && imageVal.isNotEmpty) {
      final FormPostingan formPostingan = FormPostingan(
        Title: titleVal, 
        SubTitle: subTitleVal, 
        Post: contentVal, 
        File: imageVal
      );
      
      Provider.of<FormPostinganProvider>(context, listen:false).addPost(formPostingan);
    }
    
  }

  void submitData(){
    AlertDialog alertDialog = new AlertDialog(
      title: Text("Form Postingan Informasi COVID-19"),
      content: new SizedBox(
        height: 200.0,
        child: ListView(
          children: <Widget>[
            new Text("Title: " + _titleController.text),
            new Text("Sub Title: " + _subtitleController.text),
            new Text("Content: " + _contentController.text),
            new Text("URL Images: " + _imagesController.text),
          ],
        ),
      ),
    );
    // showDialog(context: context,builder: alertDialog);
    showDialog(
      context: context, 
      builder: (BuildContext context) {
        return alertDialog;
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Tambah Postingan"),
        //backgroundColor: Colors.blue,

      ),
      drawer: MainDrawer(),
      // ignore: avoid_unnecessary_containers 
      body: Form(
        key: _formKey,
        child: ListView(
          children: 
            [Stack(
              children: [
              new Container(
                padding: new EdgeInsets.all(10.0),
                  child: Column(
                    children: <Widget>[
                      new Text(
                        "Form Postingan",
                        style: new TextStyle(fontSize: 20.0)
                      ),
                      new Text(
                        "Informasi COVID-19",
                        style: new TextStyle(fontSize: 20.0)
                      ),
                      new Padding(padding: new EdgeInsets.only(top: 30.0)),
                      new TextFormField(
                        decoration: new InputDecoration(
                          hintText: "Masukkan Judul Postingan",
                          labelText: "Title",
                          border: new OutlineInputBorder(
                            borderRadius: new BorderRadius.circular(10.0)
                          )
                        ),
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Title tidak boleh kosong';
                          }
                          return null;
                        },
                        controller: _titleController,
                      ),

                      new Padding(padding: new EdgeInsets.only(top: 20.0)),
                      new TextFormField(
                        decoration: new InputDecoration(
                          hintText: "Masukkan Sub Judul Postingan",
                          labelText: "Sub Title",
                          border: new OutlineInputBorder(
                            borderRadius: new BorderRadius.circular(10.0)
                          )
                        ),
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Sub Title tidak boleh kosong';
                          }
                          return null;
                        },
                        controller: _subtitleController,
                      ),

                      new Padding(padding: new EdgeInsets.only(top: 20.0)),
                      new TextFormField(
                        maxLines: 15,
                        decoration: new InputDecoration(
                          hintText: "Masukkan Content Postingan",
                          labelText: "Content",
                          border: new OutlineInputBorder(
                            borderRadius: new BorderRadius.circular(10.0)
                          )
                        ),
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Content tidak boleh kosong';
                          }
                          return null;
                        },
                        controller: _contentController,
                      ),

                      new Padding(padding: new EdgeInsets.only(top: 20.0)),
                      new TextFormField(
                        maxLines: 3,
                        decoration: new InputDecoration(
                          hintText: "Masukkan URL Images",
                          labelText: "Upload URL Images",
                          border: new OutlineInputBorder(
                            borderRadius: new BorderRadius.circular(10.0)
                          )
                        ),
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'URL Images tidak boleh kosong';
                          }
                          return null;
                        },
                        controller: _imagesController,
                      ),

                      new Padding(padding: new EdgeInsets.only(top: 20.0)),
                      Row(
                        
                        children: [
                          new Padding(padding: new EdgeInsets.only(right: 10.0)),
                          Expanded(
                            child: new ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                primary: Colors.yellow, // background
                                onPrimary: Colors.white, // foreground
                              ),
                              onPressed: () {
                                _titleController.clear();
                                _subtitleController.clear();
                                _contentController.clear();
                                _imagesController.clear();
                              }, 
                              child: Text("Reset")
                            ),
                          ),
                          
                          new Padding(padding: new EdgeInsets.only(right: 200.0)),
                          Expanded(
                            child: new ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                primary: Colors.green, // background
                                onPrimary: Colors.white, // foreground
                              ),
                              onPressed: () {
                                if (_formKey.currentState.validate()) {
                                  print("Title : " + _titleController.text);
                                  print("Sub Title : " + _subtitleController.text);
                                  print("Content : " + _contentController.text);
                                  print("URL Images : " + _imagesController.text);
                                }
                                onAdd();
                                Navigator.of(context).pushReplacementNamed('/');
                               
                                
                              }, 
                              child: Text("Submit")
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
              ),
              ClipPath(
                clipper: InnerClippedPart(),
                child: Container(
                  color: Color(0xff0962ff),
                  width:  MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height,
                  ),
              ),
            ],
            ),
          ]
        ),
      ),
    );
  }
}
  

class InnerClippedPart extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Path path = Path();
    //
    path.moveTo(size.width * 0.7, 0);
    path.lineTo(size.width, 0);
    path.lineTo(size.width, size.height * 0.1);
    //
    path.quadraticBezierTo(
        size.width * 0.8, size.height * 0.11, size.width * 0.7, 0);

    //
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    return true;
  }
}

class FormPostinganProvider with ChangeNotifier {
  
  List<FormPostingan> _formpost = [];

  List<FormPostingan> get formpost {
    return[..._formpost];
  }

  void addPost(FormPostingan formPostingan) async {
    const url = 'https://tk-f09.herokuapp.com/apis/v1/';
    final response=await http.post(Uri.parse(url),
    headers: {"Content-Type": "application/json"},
    body: json.encode(formPostingan));
    if(response.statusCode == 201) {
      formPostingan.id = json.decode(response.body)['id'];
      _formpost.add(formPostingan);
    }
    notifyListeners();
  }
}


class FormPostingan {
  int id;
  final String Title;
  final String SubTitle;
  final String Post;
  final String File;

 
  FormPostingan(
    {this.id, 
    this.Title, 
    this.SubTitle,
    this.Post, 
    this.File
    });

  dynamic toJson()=>{
    'id':id,
    'Title':Title,
    'SubTitle':SubTitle,
    'Post':Post,
    'File':File
  };
}