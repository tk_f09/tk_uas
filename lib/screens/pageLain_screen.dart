import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import '../widgets/main_drawer.dart';

class PageLain extends StatefulWidget {
  //const PageLain({ Key? key }) : super(key: key);
  static const routeName = '/pageLain';

  @override
  _PageLainState createState() => _PageLainState();
}

class _PageLainState extends State<PageLain> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(
        title: Text('Ini Page Lain'),
      ),
      drawer: MainDrawer(),
      body: Form(
          child: ListView(
        children: [
          new Container(
            padding: new EdgeInsets.all(10.0),
            child: Column(
              children: <Widget>[
                new Padding(padding: new EdgeInsets.only(top: 20.0)),
                new Text("Ini Page Lain", style: new TextStyle(fontSize: 20.0)),
                new Padding(padding: new EdgeInsets.only(top: 10.0)),
              ],
            ),
          ),
        ],
      )),
    );
  }
}
