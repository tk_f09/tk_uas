import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import '../widgets/main_drawer.dart';
import './input_vaksin.dart';

class SertifikatVaksin extends StatefulWidget {
  //const PageLain({ Key? key }) : super(key: key);
  static const routeName = '/sertifikat-vaksin';

  @override
  _SertifikatVaksinState createState() => _SertifikatVaksinState();
}

class _SertifikatVaksinState extends State<SertifikatVaksin> {
  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          title: Text("Sertifikat Vaksin"),
        ),
        drawer: MainDrawer(),
        body: ListView(
          padding: EdgeInsets.all(16),
          children: [
            buildImageInteractionCard(0),
            buildImageInteractionCard(0),
          ],
        ),
      );

  Widget buildImageInteractionCard(int i) => Card(
        clipBehavior: Clip.antiAlias,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(24),
        ),
        child: Column(
          children: [
            Stack(
              children: [
                Ink.image(
                  image: NetworkImage(
                    'https://asset.kompas.com/crops/GyTT_CTrnghtO51B5-trJotNGcE=/177x72:1066x665/750x500/data/photo/2021/07/05/60e250f122139.jpg',
                  ),
                  height: 240,
                  fit: BoxFit.cover,
                ),
                Positioned(
                  bottom: 16,
                  right: 16,
                  left: 16,
                  child: Text(
                    data[i]["nama"].toString(),
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.black,
                      fontSize: 24,
                    ),
                  ),
                ),
              ],
            ),
            Padding(
              padding: EdgeInsets.all(16).copyWith(bottom: 0),
              child: ListTile(
                title: Text(data[i]["nama"].toString()),
                subtitle: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                        "Tanggal Lahir    : ${data[i]["tanggalLahir"].toString()}"),
                    Text(
                        "NIK                      : ${data[i]["nik"].toString()}"),
                    Text(
                        "Tanggal Vaksin : ${data[i]["tanggalVaksin"].toString()}"),
                    Text(
                        "Lokasi Vaksin   : ${data[i]["lokasiVaksin"].toString()}"),
                  ],
                ),
                trailing:
                    Text("Vaksin ke    :  ${data[i]["vaksinKe"].toString()}"),
              ),
            ),
          ],
        ),
      );
}
