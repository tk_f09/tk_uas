import 'package:flutter/material.dart';
import 'package:tk_uas/screens/form-postingan/formPostingan_screen.dart';

import '../widgets/main_drawer.dart';
import 'home_screen.dart';
import 'pageLain_screen.dart';
import 'search_screen.dart';

class TabsScreen extends StatefulWidget {
  @override
  _TabsScreenState createState() => _TabsScreenState();
}

class _TabsScreenState extends State<TabsScreen> {
  List<Map<String, Object>> _pages;
  int _selectedPageIndex = 0;

  @override
  void initState() {
    _pages = [
      {
        'page': HomeScreen(),
        'title': 'Beranda',
      },
      {
        'page': PageLain(),
        'title': 'Kesukaan Saya',
      },
      {
        'page': SearchPage(),
        'title': 'Cari Informasi',
      },
      {
        'page': FormulirPostingan(),
        'title': 'Tambah Postingan',
      },
    ];
    super.initState();
  }

  void _selectPage(int index) {
    setState(() {
      _selectedPageIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(_pages[_selectedPageIndex]['title']),
      ),
      drawer: MainDrawer(),
      body: _pages[_selectedPageIndex]['page'],
    );
  }
}
