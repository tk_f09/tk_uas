
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class HomeScreen extends StatefulWidget {
  //const HomeScreen({ Key? key }) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final _formKey = GlobalKey<FormState>();
  final _provinsiController = TextEditingController();
  var textProvinsi = "Indonesia";
  var angkaSembuh = "4,102,700";
  var angkaPositif = "4,254,443";
  var angkaMeninggal = "143,766";
  var judulPostingan = "Fetching...";
  var deskripsiPostingan = "Fetching...";
  var linkImagePostingan =
      "https://picsum.photos/250?image=9"; //Defaut value foto

  void fetchFirstData() async {
    var result =
        await http.get(Uri.parse('https://api.kawalcorona.com/indonesia'));
    print(json.decode(result.body));
    var hasil = json.decode(result.body)[0];
    setState(() {
      angkaSembuh = hasil['sembuh'].toString();
      angkaPositif = hasil['positif'].toString();
      angkaMeninggal = hasil['meninggal'].toString();
    });
  }

  void fetchPostingan() async {
    var result =
        await http.get(Uri.parse('https://tk-f09.herokuapp.com/dataPostingan'));
    // print(json.decode(result.body));
    var hasil = json.decode(result.body)[3]['fields'];
    setState(() {
      judulPostingan = hasil['Title'];
      deskripsiPostingan = hasil['SubTitle'];
      linkImagePostingan = hasil['File'];
    });
  }

  @override
  void initState() {
    super.initState();
    fetchFirstData();
    fetchPostingan();
  }

  void getProvinsiData() async {
    var result = await http
        .get(Uri.parse('https://data.covid19.go.id/public/api/prov_list.json'));
    var listSemuaProvinsi = json.decode(result.body)['list_data'];
    var flagApakahAdaProvinsinya = false;
    for (int i = 0; i < listSemuaProvinsi.length; i++) {
      var dataProvinsi = listSemuaProvinsi[i]['key'].toLowerCase();
      var lowerControllerState = _provinsiController.text.toLowerCase();
      if (lowerControllerState == "Indonesia".toLowerCase()) {
        flagApakahAdaProvinsinya = true;
        setState(() {
          textProvinsi = "Indonesia";
          angkaSembuh = "4,102,700";
          angkaMeninggal = "4,254,443";
          angkaPositif = "143,766";
        });
      } else if (dataProvinsi.contains(lowerControllerState)) {
        print(dataProvinsi);
        print("ada nih bro!");
        flagApakahAdaProvinsinya = true;
        var buckets = listSemuaProvinsi[i]['status']['buckets'];
        print(buckets);
        setState(() {
          textProvinsi = dataProvinsi.toUpperCase();
          angkaSembuh = buckets[0]['doc_count'].toString();
          angkaMeninggal = buckets[1]['doc_count'].toString();
          angkaPositif = buckets[2]['doc_count'].toString();
        });
      }
    }
    if (flagApakahAdaProvinsinya == false) {
      setState(() {
        textProvinsi = "Provinsi tidak ditemukan";
        angkaSembuh = "0";
        angkaPositif = "0";
        angkaMeninggal = "0";
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: Form(
          key: _formKey,
          child: ListView(
            children: [
              new Container(
                padding: new EdgeInsets.all(10.0),
                child: Column(
                  children: <Widget>[
                    new Padding(padding: new EdgeInsets.only(top: 20.0)),
                    new Text("Statistik COVID-19 di Indonesia",
                        style: new TextStyle(fontSize: 20.0)),
                    new Padding(padding: new EdgeInsets.only(top: 10.0)),
                    new Text("Cari data berdasarkan Provinsi di Indonesia",
                        style: new TextStyle(fontSize: 10.0)),
                    new Padding(padding: new EdgeInsets.only(top: 20.0)),
                    new TextFormField(
                      decoration: new InputDecoration(
                          hintText: "Aceh",
                          fillColor: Colors.white,
                          filled: true,
                          labelText: "Masukkan Nama Provinsi",
                          border: new OutlineInputBorder(
                              borderRadius: new BorderRadius.circular(10.0))),
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Provinsi tidak boleh kosong';
                        }
                        return null;
                      },
                      controller: _provinsiController,
                    ),
                    new Padding(padding: new EdgeInsets.only(top: 10.0)),
                    new ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          primary: Colors.green, // background
                          onPrimary: Colors.white, // foreground
                        ),
                        onPressed: () {
                          if (_formKey.currentState.validate()) {
                            getProvinsiData();
                          }
                        },
                        child: Text("Search")),
                    new Padding(padding: new EdgeInsets.only(top: 20.0)),
                    new Text(textProvinsi,
                        style: new TextStyle(fontSize: 15.0)),
                    new Padding(padding: new EdgeInsets.only(top: 20.0)),
                    new Card(
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          ListTile(
                            title: Center(
                                child: Text('Sembuh',
                                    style: TextStyle(color: Colors.green))),
                            subtitle: Center(child: Text(angkaSembuh)),
                          ),
                        ],
                      ),
                    ),
                    new Card(
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          ListTile(
                            title: Center(
                              child: Text(
                                'Positif',
                                style: TextStyle(color: Colors.yellow),
                              ),
                            ),
                            subtitle: Center(child: Text(angkaPositif)),
                          ),
                        ],
                      ),
                    ),
                    new Card(
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          ListTile(
                            title: Center(
                                child: Text("Meninggal",
                                    style: TextStyle(color: Colors.red))),
                            subtitle: Center(child: Text(angkaMeninggal)),
                          ),
                        ],
                      ),
                    ),
                    new Padding(padding: new EdgeInsets.only(top: 20.0)),
                    new Text(
                        "Lihat informasi dan tips-tips berguna selama menghadapi masa pandemi",
                        style: new TextStyle(fontSize: 15.0)),
                    new Padding(padding: new EdgeInsets.only(top: 5.0)),
                    new Card(
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          ListTile(
                            title: Center(
                                child: Text(judulPostingan,
                                    style: TextStyle(color: Colors.black))),
                            subtitle: Center(child: Text(deskripsiPostingan)),
                            trailing: Image.network(linkImagePostingan),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ],
          )),
    );
  }
}
