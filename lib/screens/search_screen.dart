
import 'package:flutter/material.dart';
import 'package:http/http.dart'as http;
import 'dart:convert';
import '../widgets/main_drawer.dart';

class SearchPage extends StatefulWidget {
  //const SearchPage({Key key}) : super(key: key);
  static const routeName = '/search';

  @override
  State<SearchPage> createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {
  var _loadedInitData = false;
  String searched = '';

  Future<List<CardObjects>> fetchData(String query) async{
    Map<String, dynamic> curr = {};
    List<CardObjects> ret = [];

    try {
    print('testPrime');
    final response = await http.get(Uri.parse('https://tk-f09.herokuapp.com/jsonModels?search=$query'));
    print("testSecond");
    curr = jsonDecode(response.body);

    print("eught");
    //print(jsonDecode(curr['0']));

    curr.forEach((key, value) => {ret.add(CardObjects.fromJson(value))});
    
    print(ret[0].title);
    
    } catch (error) {
      print(error);
    }

    /*
    for(dynamic a in curr){
      ret.add(CardObjects.fromJson(a));
    }*/

    return ret;
  }

  @override
  Widget build(BuildContext context) {
    print("test");
    return Scaffold(
      appBar: AppBar(
        title: Text('Cari Informasi'),
      ),
      drawer: MainDrawer(),
      body: Container(
        child: FutureBuilder(
          future: fetchData(searched), 
          builder: (context, AsyncSnapshot<List<CardObjects>> snapshot) {
            /*
            print("test2");
            return Text(
              "null"
            );*/
            if(snapshot.data!=null){
            return ListView.builder(
              itemBuilder: (ctx, index) {
                
                return index == 0 ? _searchBar() :DisplayCard(
                  
                  title: snapshot.data[index-1].title,
                  paragraph: snapshot.data[index-1].paragraph,
                  image: snapshot.data[index-1].image,
                );
              },
              itemCount: snapshot.data.length+1,
            );
          } else {
            return ListView.builder(
              itemBuilder: (ctx, index){
                Text(
                  "Loading"
                );
              },
            );
          }
          
          }
        )
      )
    );
  }

  _searchBar(){
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: TextField(
        decoration: InputDecoration(
          hintText: 'Search',
        ),
      onSubmitted: (text) {
        text = text.toLowerCase();
        setState(() {
          searched = text;
        });
      },
      )
    );
  }
}



class DisplayCard extends StatelessWidget {
  final String title;
  final String paragraph;
  final String image;
  

  const DisplayCard(
    {Key key, 
      this.paragraph,
      this.title,
      this.image,
    }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return InkWell(
      //onTap: () => selectMeal(context),
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15),
        ),
        elevation: 4,
        margin: EdgeInsets.all(10),
        child: Column(
          children: <Widget>[
            SizedBox(height: 2),
            Padding(padding: EdgeInsets.all(16), child:
                Text(title, style: TextStyle(fontSize: 24))),
            Stack(
              children : [
                Ink.image(image: 
                NetworkImage(image), height : 240, fit: BoxFit.cover)
              ]
            ),
            SizedBox(height : 8),
            Padding(padding: EdgeInsets.all(16), child:
            Text(paragraph, maxLines: null,),
            ),
            SizedBox(height: 8),
          ],
        ),
      ),
    );
  }

}

class CardObjects{
  final String title;
  final String paragraph;
  final String image;

  const CardObjects(
    {Key key, 
      this.paragraph,
      this.title,
      this.image,
    });

  factory CardObjects.fromJson(Map<String, dynamic> json) {
    return CardObjects(
      title : json['Title'] as String,
      paragraph : json['Post'] as String,
      image : json['File'] as String,
    );
  }
}