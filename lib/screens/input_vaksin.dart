import 'package:flutter/material.dart';
import 'package:tk_uas/widgets/main_drawer.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';
import 'dart:convert';
//source: https://youtu.be/hfee7SIwUTs

final data = [
  {
    "nama": "Joya Ruth Amanda",
    "tanggalLahir": "23 Desember 2001",
    "nik": "123456789",
    "vaksinKe": "2",
    "tanggalVaksin": "25 Desember 2021",
    "lokasiVaksin": "Jakarta",
  }
];

class InputVaksin extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return InputVaksinState();
  }
}

class InputVaksinState extends State<InputVaksin> {
  final _formKey = GlobalKey<FormState>();
  final TextEditingController _nama = TextEditingController();
  final TextEditingController _tanggalLahir = TextEditingController();
  final TextEditingController _nik = TextEditingController();
  final TextEditingController _vaksinKe = TextEditingController();
  final TextEditingController _tanggalVaksin = TextEditingController();
  final TextEditingController _lokasiVaksin = TextEditingController();

  void onAdd() {
    final String name = _nama.text;
    final String dob = _tanggalLahir.text;
    final String nikKTP = _nik.text;
    final String jumlahVaksin = _vaksinKe.text;
    final String vaksinDate = _tanggalVaksin.text;
    final String vaksinLocation = _lokasiVaksin.text;

    if (name.isNotEmpty &&
        dob.isNotEmpty &&
        nikKTP.isNotEmpty &&
        jumlahVaksin.isNotEmpty &&
        vaksinDate.isNotEmpty &&
        vaksinLocation.isEmpty) {
      final FormVaksin formVaksin = FormVaksin(
          nama: name,
          tanggalLahir: dob,
          nik: nikKTP,
          vaksinKe: jumlahVaksin,
          tanggalVaksin: vaksinDate,
          lokasiVaksin: vaksinLocation);

      Provider.of<FormVaksinProvider>(context, listen: false)
          .daftar(formVaksin);
    }
  }

  // final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Tambah Vaksin"),
        //backgroundColor: Colors.blue,
      ),
      drawer: MainDrawer(),
      // ignore: avoid_unnecessary_containers
      body: Form(
        key: _formKey,
        child: ListView(children: [
          Stack(
            children: [
              new Container(
                padding: new EdgeInsets.all(10.0),
                child: Column(
                  children: <Widget>[
                    new Text("Form Daftar Vaksin",
                        style: new TextStyle(fontSize: 20.0)),
                    new Text("COVID-19", style: new TextStyle(fontSize: 20.0)),
                    new Padding(padding: new EdgeInsets.only(top: 30.0)),
                    new TextFormField(
                      decoration: new InputDecoration(
                          labelText: "Nama",
                          border: new OutlineInputBorder(
                              borderRadius: new BorderRadius.circular(10.0))),
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Wajib memasukan Nama!';
                        }
                        return null;
                      },
                      controller: _nama,
                    ),
                    new Padding(padding: new EdgeInsets.only(top: 20.0)),
                    new TextFormField(
                      decoration: new InputDecoration(
                          labelText: "Tanggal Lahir",
                          border: new OutlineInputBorder(
                              borderRadius: new BorderRadius.circular(10.0))),
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Wajib memasukan Tanggal Lahir!';
                        }
                        return null;
                      },
                      controller: _tanggalLahir,
                    ),
                    new Padding(padding: new EdgeInsets.only(top: 20.0)),
                    new TextFormField(
                      decoration: new InputDecoration(
                          labelText: "NIK",
                          border: new OutlineInputBorder(
                              borderRadius: new BorderRadius.circular(10.0))),
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Wajib memasukan NIK!';
                        }
                        return null;
                      },
                      controller: _nik,
                    ),
                    new Padding(padding: new EdgeInsets.only(top: 20.0)),
                    new TextFormField(
                      decoration: new InputDecoration(
                          labelText: "Vaksin ke",
                          border: new OutlineInputBorder(
                              borderRadius: new BorderRadius.circular(10.0))),
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Wajib memasukan Vaksin ke berapa!';
                        }
                        return null;
                      },
                      controller: _vaksinKe,
                    ),
                    new Padding(padding: new EdgeInsets.only(top: 20.0)),
                    new TextFormField(
                      decoration: new InputDecoration(
                          labelText: "Tanggal Vaksin",
                          border: new OutlineInputBorder(
                              borderRadius: new BorderRadius.circular(10.0))),
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Wajib memasukan Tanggal Vaksin!';
                        }
                        return null;
                      },
                      controller: _tanggalVaksin,
                    ),
                    new Padding(padding: new EdgeInsets.only(top: 20.0)),
                    new TextFormField(
                      decoration: new InputDecoration(
                          labelText: "Lokasi Vaksin",
                          border: new OutlineInputBorder(
                              borderRadius: new BorderRadius.circular(10.0))),
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Wajib memasukan Lokasi Vaksin!';
                        }
                        return null;
                      },
                      controller: _lokasiVaksin,
                    ),
                    new Padding(padding: new EdgeInsets.only(top: 20.0)),
                    Row(
                      children: [
                        new Padding(padding: new EdgeInsets.only()),
                        Expanded(
                          child: new ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                primary: Colors.green, // background
                                onPrimary: Colors.white, // foreground
                              ),
                              onPressed: () {
                                if (_formKey.currentState.validate()) {
                                  print("Nama : " + _nama.text);
                                  print("DOB : " + _tanggalLahir.text);
                                  print("NIK : " + _nik.text);
                                  print("Vaksin Ke- : " + _vaksinKe.text);
                                  print("Tanggal Vaksin : " +
                                      _tanggalVaksin.text);
                                  print(
                                      "Lokasi Vaksin : " + _lokasiVaksin.text);
                                }
                                onAdd();
                                Navigator.of(context).pushReplacementNamed('/');
                              },
                              child: Text("Submit")),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              ClipPath(
                clipper: InnerClippedPart(),
                child: Container(
                  color: Color(0xff0962ff),
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height,
                ),
              ),
            ],
          ),
        ]),
      ),
    );
  }
}
// class InputVaksinState extends State<InputVaksin> {
//   String _nama = "Joya Ruth Amanda";
//   String _tanggalLahir = "23 Desember 2001";
//   String _nik = "123456789";
//   String _vaksinKe = "2";
//   String _tanggalVaksin = "25 Desember 2021";
//   String _lokasiVaksin = "Jakarta";
//   final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

//   _buildNama() {
//     return TextFormField(
//       decoration: InputDecoration(labelText: 'Nama'),
//       maxLength: 20,
//       validator: (value) {
//         if (value.toString().isEmpty) {
//           return 'Wajib memasukan Nama!';
//         }
//       },
//       onSaved: (value) {
//         _nama = value.toString();
//       },
//     );
//   }

//   _buildTanggalLahir() {
//     return TextFormField(
//       decoration: InputDecoration(labelText: 'Tanggal Lahir'),
//       validator: (value) {
//         if (value.toString().isEmpty) {
//           return 'Wajib memasukan Tanggal Lahir!';
//         }
//       },
//       onSaved: (value) {
//         _tanggalLahir = value.toString();
//       },
//     );
//   }

//   _buildNik() {
//     return TextFormField(
//       decoration: InputDecoration(labelText: 'NIK'),
//       validator: (value) {
//         if (value.toString().isEmpty) {
//           return 'Wajib memasukan NIK!';
//         }
//       },
//       onSaved: (value) {
//         _nik = value.toString();
//       },
//     );
//   }

//   _buildVaksinKe() {
//     return TextFormField(
//       decoration: InputDecoration(labelText: 'Vaksin ke'),
//       validator: (value) {
//         if (value.toString().isEmpty) {
//           return 'Wajib memasukan Vaksin ke berapa!';
//         }
//       },
//       onSaved: (value) {
//         _vaksinKe = value.toString();
//       },
//     );
//   }

//   _buildTanggalVaksin() {
//     return TextFormField(
//       decoration: InputDecoration(labelText: 'Tanggal Vaksin'),
//       validator: (value) {
//         if (value.toString().isEmpty) {
//           return 'Wajib memasukan Tanggal Vaksin!';
//         }
//       },
//       onSaved: (value) {
//         _tanggalVaksin = value.toString();
//       },
//     );
//   }

//   _buildLokasiVaksin() {
//     return TextFormField(
//       decoration: InputDecoration(labelText: 'Lokasi Vaksin'),
//       validator: (value) {
//         if (value.toString().isEmpty) {
//           return 'Wajib memasukan Lokasi Vaksin!';
//         }
//       },
//       onSaved: (value) {
//         _lokasiVaksin = value.toString();
//       },
//     );
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//         appBar: AppBar(
//           title: Text("Input Data Vaksin"),
//         ),
//         drawer: MainDrawer(),
//         body: Container(
//           margin: EdgeInsets.all(24),
//           child: Form(
//             key: _formKey,
//             child: Column(
//               mainAxisAlignment: MainAxisAlignment.center,
//               children: [
//                 _buildNama(),
//                 _buildTanggalLahir(),
//                 _buildNik(),
//                 _buildVaksinKe(),
//                 _buildTanggalVaksin(),
//                 _buildLokasiVaksin(),
//                 SizedBox(height: 100),
//                 RaisedButton(
//                   child: Text('Submit',
//                       style: TextStyle(color: Colors.blueAccent, fontSize: 16)),
//                   onPressed: () {
//                     if (!_formKey.currentState.validate()) {
//                       return;
//                     }
//                     _formKey.currentState.save();
//                     data.add({
//                       "nama": _nama,
//                       "tanggalLahir": _tanggalLahir,
//                       "nik": _nik,
//                       "vaksinKe": _vaksinKe,
//                       "tanggalVaksin": _tanggalVaksin,
//                       "lokasiVaksin": _lokasiVaksin,
//                     });
//                     // Navigator.of(context).push(
//                     //     MaterialPageRoute(builder: (context) => Vaksin()));
//                   },
//                 ),
//               ],
//             ),
//           ),
//         ));
//   }
// }

class InnerClippedPart extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Path path = Path();
    //
    path.moveTo(size.width * 0.7, 0);
    path.lineTo(size.width, 0);
    path.lineTo(size.width, size.height * 0.1);
    //
    path.quadraticBezierTo(
        size.width * 0.8, size.height * 0.11, size.width * 0.7, 0);

    //
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    return true;
  }
}

class FormVaksinProvider with ChangeNotifier {
  List<FormVaksin> _formpost = [];

  List<FormVaksin> get formpost {
    return [..._formpost];
  }

  void daftar(FormVaksin formVaksin) async {
    const url = 'http://127.0.0.1:8000/data';
    final response = await http.post(Uri.parse(url),
        headers: {"Content-Type": "application/json"},
        body: json.encode(formVaksin));
    if (response.statusCode == 201) {
      formVaksin.id = json.decode(response.body)['id'];
      _formpost.add(formVaksin);
    }
    notifyListeners();
  }
}

class FormVaksin {
  int id;
  final String nama;
  final String tanggalLahir;
  final String nik;
  final String vaksinKe;
  final String tanggalVaksin;
  final String lokasiVaksin;

  FormVaksin(
      {this.nama,
      this.tanggalLahir,
      this.nik,
      this.vaksinKe,
      this.tanggalVaksin,
      this.lokasiVaksin});

  dynamic toJson() => {
        'Nama': nama,
        'DOB': tanggalLahir,
        'NIK': nik,
        'VaksinKe': vaksinKe,
        'Tanggal': tanggalVaksin,
        'Lokasi': lokasiVaksin
      };
}
