import 'package:flutter/material.dart';
import 'package:tk_uas/screens/SertifikatVaksin_screen.dart';
import './screens/tabs_screen.dart';
import 'screens/home_screen.dart';
import 'screens/pageLain_screen.dart';
import 'screens/search_screen.dart';
import 'package:tk_uas/screens/information_screen.dart';
import 'screens/form-postingan/formPostingan_screen.dart';
import 'screens/input_vaksin.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'TK F09',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        accentColor: Colors.indigo,
        canvasColor: Color.fromRGBO(255, 254, 229, 1),
        fontFamily: 'Raleway',
        textTheme: ThemeData.light().textTheme.copyWith(
            bodyText1: TextStyle(
              color: Color.fromRGBO(20, 51, 51, 1),
            ),
            bodyText2: TextStyle(
              color: Color.fromRGBO(20, 51, 51, 1),
            ),
            headline6: TextStyle(
              fontSize: 20,
              fontFamily: 'RobotoCondensed',
              fontWeight: FontWeight.bold,
            )),
      ),
      // home: CategoriesScreen(),
      initialRoute: '/', // default is '/'
      routes: {
        '/': (ctx) => TabsScreen(),
        PageLain.routeName: (ctx) => PageLain(),
        '/search': (ctx) => SearchPage(),
        '/information': (ctx) => Information(),
        '/form-post': (ctx) => FormulirPostingan(),
        '/input-vaksin': (ctx) => InputVaksin(),
      },
      onGenerateRoute: (settings) {
        print(settings.arguments);
      },
      onUnknownRoute: (settings) {
        return MaterialPageRoute(
          builder: (ctx) => HomeScreen(),
        );
      },
    );
  }
}
